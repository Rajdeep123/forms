# Generated by Django 2.0.5 on 2018-08-16 04:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0003_auto_20180816_1019'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='about',
            field=models.TextField(null=True),
        ),
    ]
