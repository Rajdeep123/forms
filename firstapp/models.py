from django.db import models

# Create your models here.
class user(models.Model):
    name=models.CharField(max_length=50)
    contact=models.IntegerField(unique=True)
    def __str__(self):
                 return self.name + 'contact=' + repr(self.contact)

class student(models.Model):
    BG=(
    ('A+','A+'),
    ('B+','B+'),
    ('AB+','AB+'),
    ('B+','B+'),
    ('O+','O+'),
    ('O-','O-'),
    )
    name=models.CharField(max_length=50)
    roll_no=models.IntegerField(unique=True)
    email=models.EmailField(max_length=50,unique=True)
    contact=models.IntegerField()
    Date_of_birth=models.DateField(blank=True)
    website=models.URLField()
    blood_group=models.CharField(max_length=2,choices=BG)
    about=models.TextField(null=True)
    gender=models.BooleanField("Male", blank = True, default = True)
    # genders=models.BooleanField("female", blank = True)
    def __str__(self):

        return self.name
                     # return self.name +' contact='+repr(self.contact)
# to remove s from the table name .. this name shows(Student) only to user but table name (student) stored in the database.
    class Meta:
        verbose_name_plural='Student'

class account(models.Model):
    roll_no=models.ForeignKey(student,on_delete=models.CASCADE)
    fee_paid=models.IntegerField()
    fee_pending=models.IntegerField()
    submission_date=models.DateField(auto_now_add=True)
    def __str__(self):
     return repr(models.roll_no)


class register(models.Model):
    fullname=models.CharField(max_length=50)
    password=models.CharField(max_length=50)
    email=models.EmailField(max_length=50,unique=True)
    def __str__(self):
     return repr(self.fullname)

# class signupdetails(models.model)
