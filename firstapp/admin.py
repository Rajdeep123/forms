from django.contrib import admin

# Register your models here.
from firstapp.models import user,student,account,register
class studentAdmin(admin.ModelAdmin):
    fields=['roll_no','name','Date_of_birth','website','contact','blood_group','about']
    search_fields=['name','roll_no']
    list_filter=['name','email']
    list_display=['roll_no','name','Date_of_birth','website','contact','blood_group','about']
    list_editable=['name']
admin.site.register(user)
admin.site.register(student,studentAdmin)
admin.site.register(account)
admin.site.register(register)
