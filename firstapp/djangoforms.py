from django import forms
class studentform(forms.Form):
    Name =forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Studentname....'}))
    Password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
    Confirm=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
    Email = forms.CharField(required=False,widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}))
    Date=forms.CharField(widget=forms.DateInput(attrs={'class':'form-control','type':'date'}))
    Roll_no=forms.IntegerField()
    About=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control','placeholder':'Something...'}))
    Register=forms.BooleanField(required=False)
    def clean_Name(self):
        Name=self.cleaned_data['Name']
        if len(Name)>5:
            raise forms.ValidationError("Name length should be less than 5")
        else:
            return Name
    def clean(self):
        allc=super().clean()
        p=allc['Password']
        c=allc['Confirm']
        if c!=p:
            raise forms.ValidationError("does not match")
        else:
            return c + p
# class signupform(forms.Form):
#     Name=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'students name....'}))
#     Email=forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}))
#     Password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
#     Cp=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
